package com.moizc.moviesinfoservice.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity<Map<String, List<String>>> handleRequestBodyError(WebExchangeBindException webExchangeBindException) {
        log.error("Exception: {}", webExchangeBindException.getMessage(), webExchangeBindException);
        List<String> errorMessage = webExchangeBindException.getBindingResult().getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

        Map<String, List<String>> errorMap= new HashMap<>();
        errorMap.put("message", errorMessage);

        return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
    }
}
