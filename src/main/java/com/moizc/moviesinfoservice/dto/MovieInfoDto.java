package com.moizc.moviesinfoservice.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Builder;

import java.time.LocalDate;
import java.util.List;

@Builder
public record MovieInfoDto(
        @NotBlank(message = "Movie name is required")
        String name,
        @NotNull(message = "Cast list is required")
        List<@NotBlank(message = "At least one cast name is required") String> cast,
        @NotNull(message = "Release year is required")
        @Positive(message = "Release year cannot be in negative")
        Integer releaseYear,
        LocalDate releaseDate) {
}
