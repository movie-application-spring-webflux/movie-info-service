package com.moizc.moviesinfoservice.repository;

import com.moizc.moviesinfoservice.model.MovieInfo;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface MovieInfoRepository extends ReactiveMongoRepository<MovieInfo, String> {

    Mono<MovieInfo> findByName(String movieName);

    Mono<Void> deleteByName(String movieName);
}
