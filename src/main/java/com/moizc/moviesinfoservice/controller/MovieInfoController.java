package com.moizc.moviesinfoservice.controller;

import com.moizc.moviesinfoservice.dto.MovieInfoDto;
import com.moizc.moviesinfoservice.service.MoviesInfoService;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/v1")
public class MovieInfoController {

    private final MoviesInfoService moviesInfoService;

    private final Sinks.Many<MovieInfoDto> movieInfoSink = Sinks.many().replay().all();

    public MovieInfoController(MoviesInfoService moviesInfoService) {
        this.moviesInfoService = moviesInfoService;
    }

    @PostMapping("/movie-info")
    @ResponseStatus(CREATED)
    public Mono<MovieInfoDto> addMovieInfo(@Valid @RequestBody MovieInfoDto movieInfoDto) {
        return moviesInfoService.addMovieInfo(movieInfoDto)
                .doOnNext(savedMovieInfoDto -> movieInfoSink.tryEmitNext(movieInfoDto));
    }

    @GetMapping("/movies-info")
    @ResponseStatus(OK)
    public Flux<MovieInfoDto> getAllMoviesInfo() {
        return moviesInfoService.getAllMoviesInfo();
    }

    @GetMapping(value = "/movies-info/realtime-updates", produces = MediaType.APPLICATION_NDJSON_VALUE)
    @ResponseStatus(OK)
    public Flux<MovieInfoDto> getMoviesInfoRealtime() {
        return movieInfoSink.asFlux();
    }

    @GetMapping("/movie-info/name/{movie_name}")
    public Mono<ResponseEntity<MovieInfoDto>> getMovieInfoByName(@PathVariable String movie_name) {
        return moviesInfoService.getMovieInfoByName(movie_name)
                .map(ResponseEntity.ok()::body)
                .switchIfEmpty(Mono.just(ResponseEntity.notFound().build()));
    }

    @PatchMapping("/movie-info/{movie_name}")
    public Mono<ResponseEntity<MovieInfoDto>> updateMovieInfo(@PathVariable String movie_name, @RequestBody MovieInfoDto movieInfoDto) {
        return moviesInfoService.updateMovieInfo(movie_name, movieInfoDto)
                .map(ResponseEntity.ok()::body)
                .switchIfEmpty(Mono.just(ResponseEntity.notFound().build()));
    }

    @DeleteMapping("/movie-info/{movie_name}")
    @ResponseStatus(NO_CONTENT)
    public Mono<Void> deleteMovieInfo(@PathVariable String movie_name) {
        return moviesInfoService.deleteMovieInfo(movie_name);
    }

    @GetMapping("/movie-info/id/{movie_id}")
    public Mono<ResponseEntity<MovieInfoDto>> getMovieInfoById(@PathVariable String movie_id) {
        return moviesInfoService.getMovieInfoById(movie_id)
                .map(ResponseEntity.ok()::body)
                .switchIfEmpty(Mono.just(ResponseEntity.notFound().build()));
    }

}
