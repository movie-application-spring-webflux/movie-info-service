package com.moizc.moviesinfoservice.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;

@Data
@Document
@Builder
public class MovieInfo {

    @Id
    private String id;

    private String name;

    private List<String> cast;

    private Integer releaseYear;

    private LocalDate releaseDate;
}
