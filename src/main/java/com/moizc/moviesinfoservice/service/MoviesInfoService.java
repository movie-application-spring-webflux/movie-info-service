package com.moizc.moviesinfoservice.service;

import com.moizc.moviesinfoservice.dto.MovieInfoDto;
import com.moizc.moviesinfoservice.model.MovieInfo;
import com.moizc.moviesinfoservice.repository.MovieInfoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MoviesInfoService {

    private final MovieInfoRepository movieInfoRepository;

    public MoviesInfoService(MovieInfoRepository movieInfoRepository) {
        this.movieInfoRepository = movieInfoRepository;
    }

    public Mono<MovieInfoDto> addMovieInfo(MovieInfoDto movieInfoDto) {
        return movieInfoRepository.save(getMovieInfo(movieInfoDto))
                .flatMap(this::getMovieInfoDto);
    }

    private MovieInfo getMovieInfo(MovieInfoDto movieInfoDto) {
        return MovieInfo.builder()
                .name(movieInfoDto.name())
                .releaseDate(movieInfoDto.releaseDate())
                .cast(movieInfoDto.cast())
                .releaseYear(movieInfoDto.releaseYear())
                .build();
    }

    private Mono<MovieInfoDto> getMovieInfoDto(MovieInfo movieInfo) {
        return Mono.just(MovieInfoDto.builder()
                .releaseYear(movieInfo.getReleaseYear())
                .name(movieInfo.getName())
                .releaseDate(movieInfo.getReleaseDate())
                .cast(movieInfo.getCast())
                .build());
    }

    public Flux<MovieInfoDto> getAllMoviesInfo() {
        return movieInfoRepository.findAll()
                .flatMap(this::getMovieInfoDto);
    }

    public Mono<MovieInfoDto> getMovieInfoByName(String movieName) {
        return movieInfoRepository.findByName(movieName)
                .flatMap(this::getMovieInfoDto);
    }

    public Mono<MovieInfoDto> updateMovieInfo(String movieName, MovieInfoDto movieInfoDto) {
        return movieInfoRepository.findByName(movieName)
                .flatMap(movieInfo -> {
                    movieInfo.setCast(movieInfoDto.cast());
                    movieInfo.setName(movieInfoDto.name());
                    movieInfo.setReleaseDate(movieInfoDto.releaseDate());
                    movieInfo.setReleaseYear(movieInfoDto.releaseYear());

                    return movieInfoRepository.save(movieInfo)
                            .flatMap(this::getMovieInfoDto);
                });
    }

    public Mono<Void> deleteMovieInfo(String movieName) {
        return movieInfoRepository.deleteByName(movieName);
    }

    public Mono<MovieInfoDto> getMovieInfoById(String movieId) {
        return movieInfoRepository.findById(movieId)
                .flatMap(this::getMovieInfoDto);
    }
}
