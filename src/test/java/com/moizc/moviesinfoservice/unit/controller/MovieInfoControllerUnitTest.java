package com.moizc.moviesinfoservice.unit.controller;

import com.moizc.moviesinfoservice.controller.MovieInfoController;
import com.moizc.moviesinfoservice.dto.MovieInfoDto;
import com.moizc.moviesinfoservice.service.MoviesInfoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

@WebFluxTest(controllers = MovieInfoController.class)
@AutoConfigureWebTestClient
public class MovieInfoControllerUnitTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private MoviesInfoService moviesInfoService;

    @Nested
    class SaveMovieInfo {
        @Test
        void shouldSaveMovieInfo() {
            //arrange
            MovieInfoDto movieInfoDto = MovieInfoDto.builder()
                    .name("Spiderman")
                    .cast(List.of("Tom Holland", "Zendaya"))
                    .releaseDate(LocalDate.of(2022, 2, 15))
                    .releaseYear(2022)
                    .build();

            Mono<MovieInfoDto> movieInfoDtoMono = Mono.just(movieInfoDto);

            when(moviesInfoService.addMovieInfo(isA(MovieInfoDto.class))).thenReturn(movieInfoDtoMono);

            webTestClient.post().uri("/v1/movie-info")
                    .bodyValue(movieInfoDto)
                    .exchange()
                    .expectStatus()
                    .isCreated()
                    .expectBody(MovieInfoDto.class)
                    .consumeWith(movieInfoDtoEntityExchangeResult -> {
                        MovieInfoDto actualMovieInfoDto = movieInfoDtoEntityExchangeResult.getResponseBody();
                        assert actualMovieInfoDto != null;
                        verify(moviesInfoService, times(1)).addMovieInfo(isA(MovieInfoDto.class));
                    });
        }

        @Test
        void shouldThrowErrorWhenValidationFailsForMovieInfoRequestBody() {
            //arrange
            MovieInfoDto movieInfoDto = MovieInfoDto.builder()
                    .name(null)
                    .cast(null)
                    .releaseDate(LocalDate.of(2022, 2, 15))
                    .releaseYear(-2022)
                    .build();

            webTestClient.post().uri("/v1/movie-info")
                    .bodyValue(movieInfoDto)
                    .exchange()
                    .expectStatus()
                    .isBadRequest()
                    .expectBody(new ParameterizedTypeReference<Map<String, List<String>>>() {
                    })
                    .consumeWith(mapEntityExchangeResult -> {
                        Map<String, List<String>> responseBody = mapEntityExchangeResult.getResponseBody();
                        assert responseBody != null;

                        List<String> errorMessage = responseBody.get("message");
                        List<String> sortedErrorMessage = errorMessage.stream().sorted().toList();

                        Assertions.assertEquals(3, errorMessage.size());
                        Assertions.assertEquals(sortedErrorMessage.get(0), "Cast list is required");
                        Assertions.assertEquals(sortedErrorMessage.get(1), "Movie name is required");
                        Assertions.assertEquals(sortedErrorMessage.get(2), "Release year cannot be in negative");
                    });
        }
    }

    @Test
    void shouldGetAllMoviesInfo() {
        //arrange
        MovieInfoDto avengers = MovieInfoDto.builder()
                .releaseYear(2010)
                .releaseDate(LocalDate.of(2010, 11, 18))
                .cast(List.of("RDJ", "Chris Evans", "Paul Rudd"))
                .name("Avengers")
                .build();

        MovieInfoDto avengersAgeOfUltron = MovieInfoDto.builder()
                .releaseYear(2015)
                .releaseDate(LocalDate.of(2015, 11, 18))
                .cast(List.of("Samuel Jackson", "Mark Ruffalo", "Scarlett Johansson"))
                .name("Avengers: Age of Ultron")
                .build();

        when(moviesInfoService.getAllMoviesInfo()).thenReturn(Flux.just(avengers, avengersAgeOfUltron));

        //act
        webTestClient.get()
                .uri("/v1/movies-info")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(MovieInfoDto.class)
                .hasSize(2);
    }

    @Test
    void shouldGetMovieByName() {
        //arrange
        String movieName = "Avengers Infinity War";

        MovieInfoDto avengersInfinityWar = MovieInfoDto.builder()
                .releaseYear(2010)
                .releaseDate(LocalDate.of(2019, 11, 18))
                .cast(List.of("RDJ", "Chris Evans", "Paul Rudd", "James Brolin"))
                .name(movieName)
                .build();

        when(moviesInfoService.getMovieInfoByName(isA(String.class))).thenReturn(Mono.just(avengersInfinityWar));

        //act
        webTestClient.get().uri("/v1/movie-info/" + movieName)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(MovieInfoDto.class)
                .consumeWith(movieInfoDtoEntityExchangeResult -> {
                    MovieInfoDto responseBody = movieInfoDtoEntityExchangeResult.getResponseBody();
                    assert responseBody != null;
                    Assertions.assertEquals(movieName, responseBody.name());
                });
    }

    @Test
    void shouldUpdateAMovieInfo() {
        //arrange
        String movieName = "Avengers Infinity War";

        int releaseYear = 2010;
        MovieInfoDto avengersInfinityWar = MovieInfoDto.builder()
                .releaseYear(releaseYear)
                .releaseDate(LocalDate.of(2018, 11, 18))
                .cast(List.of("RDJ", "Chris Evans", "Paul Rudd", "James Brolin"))
                .name(movieName)
                .build();

        when(moviesInfoService.updateMovieInfo(movieName, avengersInfinityWar)).thenReturn(Mono.just(avengersInfinityWar));

        //act
        webTestClient.patch().uri("/v1/movie-info/" + movieName)
                .bodyValue(avengersInfinityWar)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(MovieInfoDto.class)
                .consumeWith(movieInfoDtoEntityExchangeResult -> {
                    MovieInfoDto responseBody = movieInfoDtoEntityExchangeResult.getResponseBody();
                    assert responseBody != null;
                });
    }

    @Test
    void shouldDeleteMovieInfo() {
        //arrange
        String movieName = "Avengers Infinity War";

        when(moviesInfoService.deleteMovieInfo(isA(String.class))).thenReturn(Mono.empty());

        //act
        webTestClient.delete().uri("/v1/movie-info/" + movieName)
                .exchange()
                .expectStatus()
                .isNoContent();
    }
}
