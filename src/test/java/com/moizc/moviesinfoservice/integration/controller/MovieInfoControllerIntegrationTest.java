package com.moizc.moviesinfoservice.integration.controller;

import com.moizc.moviesinfoservice.dto.MovieInfoDto;
import com.moizc.moviesinfoservice.model.MovieInfo;
import com.moizc.moviesinfoservice.repository.MovieInfoRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureWebTestClient
class MovieInfoControllerIntegrationTest {

    @Autowired
    private MovieInfoRepository movieInfoRepository;

    @Autowired
    private WebTestClient webTestClient;

    @AfterEach
    void tearDown() {
        movieInfoRepository.deleteAll().block();
    }

    @Test
    void shouldSaveMovieInfo() {
        //arrange
        MovieInfoDto expectedMovieDto = MovieInfoDto.builder()
                .name("Spiderman")
                .cast(List.of("Tom Holland", "Zendaya"))
                .releaseDate(LocalDate.of(2022, 2, 15))
                .releaseYear(2022)
                .build();

        //act
        Flux<MovieInfoDto> actualMovieDto = webTestClient.post().uri("/v1/movie-info")
                .bodyValue(expectedMovieDto)
                .exchange()
                .expectStatus()
                .isCreated()
                .returnResult(MovieInfoDto.class)
                .getResponseBody();

        //assert
        StepVerifier.create(actualMovieDto)
                .expectSubscription()
                .expectNext(expectedMovieDto)
                .verifyComplete();
    }

    @Nested
    class GetMovieInfo {
        @Test
        void shouldGetAllMoviesInfo() {
            //arrange

            MovieInfo avengers = MovieInfo.builder()
                    .releaseYear(2010)
                    .releaseDate(LocalDate.of(2010, 11, 18))
                    .cast(List.of("RDJ", "Chris Evans", "Paul Rudd"))
                    .name("Avengers")
                    .build();

            MovieInfo avengersAgeOfUltron = MovieInfo.builder()
                    .releaseYear(2015)
                    .releaseDate(LocalDate.of(2015, 11, 18))
                    .cast(List.of("Samuel Jackson", "Mark Ruffalo", "Scarlett Johansson"))
                    .name("Avengers: Age of Ultron")
                    .build();

            movieInfoRepository.saveAll(List.of(avengers, avengersAgeOfUltron)).blockFirst();

            //act
            Flux<MovieInfoDto> responseBody = webTestClient.get()
                    .uri("/v1/movies-info")
                    .exchange()
                    .expectStatus()
                    .isOk()
                    .returnResult(MovieInfoDto.class)
                    .getResponseBody();

            //assert
            StepVerifier.create(responseBody)
                    .expectSubscription()
                    .assertNext(movieInfoDto -> {
                        Assertions.assertEquals(avengers.getName(), movieInfoDto.name());
                        Assertions.assertEquals(avengers.getCast(), movieInfoDto.cast());
                        Assertions.assertEquals(avengers.getReleaseYear(), movieInfoDto.releaseYear());
                        Assertions.assertEquals(avengers.getReleaseDate(), movieInfoDto.releaseDate());
                    })
                    .assertNext(movieInfoDto -> {
                        Assertions.assertEquals(avengersAgeOfUltron.getName(), movieInfoDto.name());
                        Assertions.assertEquals(avengersAgeOfUltron.getCast(), movieInfoDto.cast());
                        Assertions.assertEquals(avengersAgeOfUltron.getReleaseYear(), movieInfoDto.releaseYear());
                        Assertions.assertEquals(avengersAgeOfUltron.getReleaseDate(), movieInfoDto.releaseDate());
                    })
                    .verifyComplete();
        }

        @Test
        void shouldGetMovieByName() {
            //arrange
            String movieName = "Avengers Infinity War";

            int releaseYear = 2010;
            MovieInfo avengersInfinityWar = MovieInfo.builder()
                    .releaseYear(releaseYear)
                    .releaseDate(LocalDate.of(2019, 11, 18))
                    .cast(List.of("RDJ", "Chris Evans", "Paul Rudd", "James Brolin"))
                    .name(movieName)
                    .build();

            movieInfoRepository.save(avengersInfinityWar).block();

            //act

            Flux<MovieInfoDto> responseBody = webTestClient.get().uri("/v1/movie-info/" + movieName)
                    .exchange()
                    .expectStatus()
                    .isOk()
                    .returnResult(MovieInfoDto.class)
                    .getResponseBody();

            //assert
            StepVerifier.create(responseBody)
                    .expectSubscription()
                    .assertNext(movieInfoDto -> {
                        Assertions.assertEquals(avengersInfinityWar.getReleaseDate(), movieInfoDto.releaseDate());
                        Assertions.assertEquals(avengersInfinityWar.getName(), movieInfoDto.name());
                        Assertions.assertEquals(avengersInfinityWar.getCast(), movieInfoDto.cast());
                        Assertions.assertEquals(avengersInfinityWar.getReleaseYear(), movieInfoDto.releaseYear());
                    })
                    .verifyComplete();
        }

        @Test
        void shouldGiveNotFoundStatusWhenMovieIsNotPresentInDatabase() {
            //arrange
            String movieName = "Avengers Infinity War";

            webTestClient.get().uri("/v1/movie-info/" + movieName)
                    .exchange()
                    .expectStatus()
                    .isNotFound();
        }
    }

    @Nested
    class UpdateMovieInfo {
        @Test
        void shouldUpdateAMovieInfo() {
            //arrange
            String movieName = "Avengers Infinity War";

            int releaseYear = 2010;
            MovieInfo avengersInfinityWar = MovieInfo.builder()
                    .releaseYear(releaseYear)
                    .releaseDate(LocalDate.of(2018, 11, 18))
                    .cast(List.of("RDJ", "Chris Evans", "Paul Rudd", "James Brolin"))
                    .name(movieName)
                    .build();

            movieInfoRepository.save(avengersInfinityWar).block();

            LocalDate updatedReleaseDate = LocalDate.of(2019, 11, 18);

            MovieInfoDto avengersInfinityWarDto = MovieInfoDto.builder()
                    .name(movieName)
                    .cast(List.of("RDJ", "Chris Evans", "Paul Rudd", "James Brolin"))
                    .releaseYear(releaseYear)
                    .releaseDate(updatedReleaseDate)
                    .build();

            //act
            Flux<MovieInfoDto> responseBody = webTestClient.patch().uri("/v1/movie-info/" + movieName)
                    .bodyValue(avengersInfinityWarDto)
                    .exchange()
                    .expectStatus()
                    .isOk()
                    .returnResult(MovieInfoDto.class)
                    .getResponseBody();

            //assert
            StepVerifier.create(responseBody)
                    .expectSubscription()
                    .assertNext(movieInfoDto -> {
                        Assertions.assertEquals(updatedReleaseDate, movieInfoDto.releaseDate());
                        Assertions.assertEquals(avengersInfinityWar.getName(), movieInfoDto.name());
                        Assertions.assertEquals(avengersInfinityWar.getCast(), movieInfoDto.cast());
                        Assertions.assertEquals(avengersInfinityWar.getReleaseYear(), movieInfoDto.releaseYear());
                    })
                    .verifyComplete();
        }

        @Test
        void shouldReturnNotFoundWhenMovieNameIsNotPresentInDatabase() {
            //arrange
            String movieName = "Avengers Infinity War";

            MovieInfoDto avengersInfinityWarDto = MovieInfoDto.builder()
                    .name(movieName)
                    .cast(List.of("RDJ", "Chris Evans", "Paul Rudd", "James Brolin"))
                    .releaseYear(2020)
                    .releaseDate(LocalDate.of(2020, 11, 18))
                    .build();

            //act
            webTestClient.patch().uri("/v1/movie-info/" + movieName)
                    .bodyValue(avengersInfinityWarDto)
                    .exchange()
                    .expectStatus()
                    .isNotFound();
        }
    }

    @Test
    void shouldDeleteMovieInfo() {
        //arrange
        String movieName = "Avengers Infinity War";

        int releaseYear = 2010;
        MovieInfo avengersInfinityWar = MovieInfo.builder()
                .releaseYear(releaseYear)
                .releaseDate(LocalDate.of(2018, 11, 18))
                .cast(List.of("RDJ", "Chris Evans", "Paul Rudd", "James Brolin"))
                .name(movieName)
                .build();

        movieInfoRepository.save(avengersInfinityWar).block();

        //act
        webTestClient.delete().uri("/v1/movie-info/" + movieName)
                .exchange()
                .expectStatus()
                .isNoContent();
    }
}